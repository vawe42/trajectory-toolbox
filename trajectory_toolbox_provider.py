# -*- coding: utf-8 -*-

"""
/***************************************************************************
 TrajectoryToolbox
                                 A QGIS plugin
 This plugin provides a number of tools for trajectory analysis based on the
 PostGIS LinestringM data type with a focus on polygonal context annotation.
 This implementation is aimed at AIS movement data from the Danish Maritime
 Authority, which can be downloaded from ftp://ftp.ais.dk/ais_data/
 The preprocessing tools in the toolbox allow to split the data into single
 trips / trajectories, convert them to LinestringM format and annotate them
 with polygonal context data. The Analysis tools enable trajectory analysis
 with instant loading of the resulting layers.

 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2018-06-11
        copyright            : (C) 2018 by Eva Westermeier
        email                : eva dot westermeier at mailbox dot org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = 'Eva Westermeier'
__date__ = '2018-06-11'
__copyright__ = '(C) 2018 by Eva Westermeier'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import QgsProcessingProvider
from .detectstops import DetectStops
from .converttrajectories import ConvertTrajectories
from .annotatetrajectories import AnnotateTrajectories
from .getenterpoints import GetEnterPoints

class TrajectoryToolboxProvider(QgsProcessingProvider):

    def __init__(self):
        QgsProcessingProvider.__init__(self)

        # Load algorithms
        self.alglist = [DetectStops(), ConvertTrajectories(), AnnotateTrajectories(),
                        GetEnterPoints()]

    def unload(self):
        """
        Unloads the provider. Any tear-down steps required by the provider
        should be implemented here.
        """
        pass

    def loadAlgorithms(self):
        """
        Loads all algorithms belonging to this provider.
        """
        for alg in self.alglist:
            self.addAlgorithm( alg )

    def id(self):
        """
        Returns the unique provider id, used for identifying the provider. This
        string should be a unique, short, character only string, eg "qgis" or
        "gdal". This string should not be localised.
        """
        return 'trajectorytbx'

    def name(self):
        """
        Returns the provider name, which is used to describe the provider
        within the GUI.

        This string should be short (e.g. "Lastools") and localised.
        """
        return self.tr('Trajectory Toolbox')

    def longName(self):
        """
        Returns the a longer version of the provider name, which can include
        extra details such as version numbers. E.g. "Lastools LIDAR tools
        (version 2.2.1)". This string should be localised. The default
        implementation returns the same string as name().
        """
        return self.name()
