# Trajectory Toolbox

A python processing provider plugin for QGIS 3.x. This plugin provides a number of tools for trajectory analysis based on the PostGIS LinestringM data type with a focus on polygonal context annotation.

This implementation is aimed at AIS movement data from the Danish Maritime Authority, which can be downloaded from ftp://ftp.ais.dk/ais_data/ The preprocessing tools in the toolbox allow to split the data into single trips i.e. trajectories, convert them to LinestringM format and annotate them with polygonal context data. The Analysis tools enable trajectory analysis with instant loading of the resulting layers.
